﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simple_Disk_Catalog;

namespace sqliteTest
{
    class SqliteTransactionUsage
    {
        public void TestTransaction()
        {
            string ID = "";
            var sqLiteDatabase = new Simple_Disk_Catalog.SQLiteDatabase(true);

            sqLiteDatabase.Delete("Disks", string.Format(" id = '{0}'", ID));

            sqLiteDatabase.Delete("Folders", string.Format(" DiskID = '{0}'", ID));

            sqLiteDatabase.Delete("Files", string.Format(" DiskID = '{0}'", ID));

            sqLiteDatabase.Delete("Loan", string.Format(" DiskID = '{0}'", ID));

            sqLiteDatabase.CommitTransaction();
        }
    }
}
