﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sqliteTest
{
    public class RecipeModel
    {
        public int RecipeID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int PreparationTime { get; set; }

        public int CookingTime { get; set; }

        SQLiteDatabase db;

        public void GetData()
        {
            try
            {
                db = new SQLiteDatabase();
                DataTable recipe;
                String query = "select NAME \"Name\", DESCRIPTION \"Description\",";
                query += "PREP_TIME \"Prep Time\", COOKING_TIME \"Cooking Time\"";
                query += "from RECIPE;";
                recipe = db.GetDataTable(query);
            }
            catch (Exception fail)
            {
                String error = "The following error has occurred:\n\n";
                error += fail.Message.ToString() + "\n\n";
            }
        }

        public void InsertData()
        {
            db = new SQLiteDatabase();
            Dictionary<String, String> data = new Dictionary<String, String>();
            data.Add("NAME", "Burger");
            data.Add("DESCRIPTION", "Veg burger");
            data.Add("PREP_TIME", "20");
            data.Add("COOKING_TIME", "15");
            data.Add("COOKING_DIRECTIONS", "Placeholder");
            try
            {
                db.Insert("RECIPE", data);
            }
            catch (Exception crap)
            {

            }
        }

        public void UpdateData()
        {
            db = new SQLiteDatabase();
            Dictionary<String, String> data = new Dictionary<String, String>();
            DataTable rows;
            data.Add("NAME", "Burger");
            data.Add("DESCRIPTION", "Veg burger");
            data.Add("PREP_TIME", "25");
            data.Add("COOKING_TIME", "15");
            try
            {
                db.Update("RECIPE", data, String.Format("RECIPE.ID = {0}", 1));
            }
            catch (Exception crap)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void DeleteData()
        {
            try
            {
                db = new SQLiteDatabase();
                String recipeID = "1";
                db.Delete("RECIPE", String.Format("ID = {0}", recipeID));
                db.Delete("HAS_INGREDIENT", String.Format("ID = {0}", recipeID));
            }
            catch (Exception fail)
            {
            }
        }
    }
}
